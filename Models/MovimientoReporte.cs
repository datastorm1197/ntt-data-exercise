﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ntt_Data_Exercise.Models
{
    public class MovimientoReporte
    {
        public string Fecha { get; set; }
        public string Cliente { get; set; }
        public string NroCuenta { get; set; }
        public string TipoCuenta { get; set; }
        public decimal SaldoInicial { get; set; }
        public decimal SaldoDisponible { get; set; }
        public string Estado { get; set; }
        public string Movimiento { get; set; }
    }
}