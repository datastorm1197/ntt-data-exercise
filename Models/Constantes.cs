﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ntt_Data_Exercise.Models
{
    public class Constantes
    {
        public static class TipoMovimiento
        {
            public static string Credito = "Crédito";
            public static string Debito = "Dédito";
        }

        public static class ValoresTope
        {
            public static int MaximoDiario = 1000;
        }

        public static class Formatos
        {
            public static string DateFormat = "dd/mm/yyyy";
        }

        public static class CuentaEstado
        {
            public static string Activa = "ACTIVA";
            public static string Inactiva = "INACTIVA";
        }
    }
}