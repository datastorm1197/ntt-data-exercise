﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ntt_Data_Exercise;
using Ntt_Data_Exercise.Models;
using static Ntt_Data_Exercise.Models.Constantes;

namespace Ntt_Data_Exercise.Controllers
{
    public class MovimientosController : Controller
    {
        private Ntt_Data_ExerciseEntities db = new Ntt_Data_ExerciseEntities();

        // GET: Movimientos
        public ActionResult Index()
        {
            var movimientos = db.Movimientos.Include(m => m.Cuenta);
            return View(movimientos.ToList());
        }

        // GET: Movimientos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimientos movimientos = db.Movimientos.Find(id);
            if (movimientos == null)
            {
                return HttpNotFound();
            }
            return View(movimientos);
        }

        // GET: Movimientos/Create
        public ActionResult Create()
        {
            ViewBag.CuentaId = new SelectList(db.Cuenta, "Id", "NroCuenta");
            return View();
        }

        // POST: Movimientos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CuentaId,Fecha,TipoMovimiento,Valor,Saldo")] Movimientos movimientos)
        {
            if (ModelState.IsValid)
            {
                var ultimoMovimiento = db.Movimientos.Where(m => m.CuentaId == movimientos.CuentaId).OrderBy(p => p.Fecha).ToList().LastOrDefault();

                // Cuando se crea el primer movimiento
                if (ultimoMovimiento == null)
                {
                    var saldoInicial = db.Cuenta.Where(p => p.Id == movimientos.CuentaId).First().SaldoInicial;
                    if (movimientos.Valor < 0)
                    {
                        movimientos.TipoMovimiento = TipoMovimiento.Debito;
                        movimientos.Saldo = saldoInicial - movimientos.Valor;
                    }
                    else
                    {
                        movimientos.TipoMovimiento = TipoMovimiento.Credito;
                        movimientos.Saldo = saldoInicial + movimientos.Valor;
                    }
                } 
                // Cuando ya existe al menos un movimiento
                else
                {
                    // Creación de débitos
                    if (movimientos.Valor < 0)
                    {
                        if (ultimoMovimiento.Saldo <= 0)
                        {
                            ModelState.AddModelError("Valor", "No existe saldo suficiente");
                        }

                        var totalMovimientosDiaActual = db.Movimientos.Where(m => m.Fecha.Value == DateTime.Today 
                                                                       && m.TipoMovimiento == TipoMovimiento.Debito)
                                                                      .Sum(p => p.Valor);

                        if (totalMovimientosDiaActual > (ValoresTope.MaximoDiario * -1))
                        {
                            ModelState.AddModelError("Valor", "Se ha excedido el valor máximo.");
                        }

                        movimientos.TipoMovimiento = TipoMovimiento.Debito;
                        movimientos.Saldo = ultimoMovimiento.Saldo + movimientos.Valor;
                    }
                    // Creación de créditos
                    else
                    {

                        movimientos.TipoMovimiento = TipoMovimiento.Credito;
                        movimientos.Saldo = ultimoMovimiento.Saldo + movimientos.Valor;
                    }
                }


                db.Movimientos.Add(movimientos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CuentaId = new SelectList(db.Cuenta, "Id", "NroCuenta", movimientos.CuentaId);
            return View(movimientos);
        }
        
        // Método que retorna la lista de movimientos de una cuenta dados un rango de fechas
        // GET: Movimientos/ReporteEstadoCuenta
        [HttpPost]
        public JsonResult ReporteEstadoCuenta(int cuentaId, DateTime fechaInicial, DateTime fechaFinal)
        {
            var movList = (from m in db.Movimientos
                           join c in db.Cuenta on m.CuentaId equals c.Id
                           join cl in db.Cliente on c.ClienteId equals cl.Id
                           join p in db.Persona on cl.PersonaId equals p.Id
                           where m.CuentaId == cuentaId
                           && m.Fecha.Value >= fechaInicial && m.Fecha <= fechaFinal
                           orderby m.Fecha descending
                           select new MovimientoReporte
                           {
                               Cliente = p.Nombre,
                               Fecha = m.Fecha.HasValue ? m.Fecha.Value.ToString() : string.Empty,
                               SaldoInicial = c.SaldoInicial ?? 0,
                               SaldoDisponible = m.Saldo ?? 0,
                               NroCuenta = c.NroCuenta,
                               Movimiento = m.TipoMovimiento,
                               Estado = c.Estado == 1 ? CuentaEstado.Activa : CuentaEstado.Inactiva
                           }).ToList();

            return new JsonResult() { 
                Data = movList,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        // GET: Movimientos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimientos movimientos = db.Movimientos.Find(id);
            if (movimientos == null)
            {
                return HttpNotFound();
            }
            ViewBag.CuentaId = new SelectList(db.Cuenta, "Id", "NroCuenta", movimientos.CuentaId);
            return View(movimientos);
        }

        // POST: Movimientos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CuentaId,Fecha,TipoMovimiento,Valor,Saldo")] Movimientos movimientos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(movimientos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CuentaId = new SelectList(db.Cuenta, "Id", "NroCuenta", movimientos.CuentaId);
            return View(movimientos);
        }

        // GET: Movimientos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimientos movimientos = db.Movimientos.Find(id);
            if (movimientos == null)
            {
                return HttpNotFound();
            }
            return View(movimientos);
        }

        // POST: Movimientos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Movimientos movimientos = db.Movimientos.Find(id);
            db.Movimientos.Remove(movimientos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
