create database Ntt_Data_Exercise

USE [Ntt_Data_Exercise]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 9/4/2022 13:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonaId] [int] NULL,
	[PasswordCliente] [varchar](100) NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cuenta]    Script Date: 9/4/2022 13:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cuenta](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClienteId] [int] NULL,
	[NroCuenta] [varchar](20) NULL,
	[SaldoInicial] [decimal](7, 2) NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movimientos]    Script Date: 9/4/2022 13:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movimientos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CuentaId] [int] NULL,
	[Fecha] [datetime] NULL,
	[TipoMovimiento] [varchar](15) NULL,
	[Valor] [decimal](7, 2) NULL,
	[Saldo] [decimal](7, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Persona]    Script Date: 9/4/2022 13:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Persona](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[Genero] [varchar](2) NULL,
	[Edad] [int] NULL,
	[Identificacion] [varchar](15) NULL,
	[Direccion] [varchar](100) NULL,
	[Telefono] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cliente] ON 
GO
INSERT [dbo].[Cliente] ([Id], [PersonaId], [PasswordCliente], [Estado]) VALUES (1, 2, N'1234321', 1)
GO
INSERT [dbo].[Cliente] ([Id], [PersonaId], [PasswordCliente], [Estado]) VALUES (3, 1, N'3214123', 1)
GO
SET IDENTITY_INSERT [dbo].[Cliente] OFF
GO
SET IDENTITY_INSERT [dbo].[Cuenta] ON 
GO
INSERT [dbo].[Cuenta] ([Id], [ClienteId], [NroCuenta], [SaldoInicial], [Estado]) VALUES (1, 3, N'2889585', CAST(25.00 AS Decimal(7, 2)), 1)
GO
INSERT [dbo].[Cuenta] ([Id], [ClienteId], [NroCuenta], [SaldoInicial], [Estado]) VALUES (2, 3, N'18298787', CAST(23.00 AS Decimal(7, 2)), 1)
GO
SET IDENTITY_INSERT [dbo].[Cuenta] OFF
GO
SET IDENTITY_INSERT [dbo].[Movimientos] ON 
GO
INSERT [dbo].[Movimientos] ([Id], [CuentaId], [Fecha], [TipoMovimiento], [Valor], [Saldo]) VALUES (2, 1, CAST(N'2022-04-09T00:00:00.000' AS DateTime), N'Crédito', CAST(35.00 AS Decimal(7, 2)), CAST(60.00 AS Decimal(7, 2)))
GO
INSERT [dbo].[Movimientos] ([Id], [CuentaId], [Fecha], [TipoMovimiento], [Valor], [Saldo]) VALUES (3, 1, CAST(N'2022-04-09T00:00:00.000' AS DateTime), N'Dédito', CAST(-10.00 AS Decimal(7, 2)), CAST(50.00 AS Decimal(7, 2)))
GO
INSERT [dbo].[Movimientos] ([Id], [CuentaId], [Fecha], [TipoMovimiento], [Valor], [Saldo]) VALUES (4, 1, CAST(N'2022-04-09T00:00:00.000' AS DateTime), N'Dédito', CAST(-20.00 AS Decimal(7, 2)), CAST(30.00 AS Decimal(7, 2)))
GO
INSERT [dbo].[Movimientos] ([Id], [CuentaId], [Fecha], [TipoMovimiento], [Valor], [Saldo]) VALUES (5, 1, CAST(N'2022-04-09T00:00:00.000' AS DateTime), N'Dédito', CAST(-30.00 AS Decimal(7, 2)), CAST(0.00 AS Decimal(7, 2)))
GO
INSERT [dbo].[Movimientos] ([Id], [CuentaId], [Fecha], [TipoMovimiento], [Valor], [Saldo]) VALUES (6, 1, CAST(N'2022-04-09T00:00:00.000' AS DateTime), N'Dédito', CAST(-10.00 AS Decimal(7, 2)), CAST(-10.00 AS Decimal(7, 2)))
GO
SET IDENTITY_INSERT [dbo].[Movimientos] OFF
GO
SET IDENTITY_INSERT [dbo].[Persona] ON 
GO
INSERT [dbo].[Persona] ([Id], [Nombre], [Genero], [Edad], [Identificacion], [Direccion], [Telefono]) VALUES (1, N'Marco', N'M', 12, N'100879877987', N'Test', N'72364187')
GO
INSERT [dbo].[Persona] ([Id], [Nombre], [Genero], [Edad], [Identificacion], [Direccion], [Telefono]) VALUES (2, N'Andrés Jácome', N'M', 24, N'1004322697', N'Atuntaqui', N'94598')
GO
INSERT [dbo].[Persona] ([Id], [Nombre], [Genero], [Edad], [Identificacion], [Direccion], [Telefono]) VALUES (3, N'Estafanía Calderón', N'F', 72, N'109487988', N'Ibarra', N'0892897')
GO
SET IDENTITY_INSERT [dbo].[Persona] OFF
GO
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD FOREIGN KEY([PersonaId])
REFERENCES [dbo].[Persona] ([Id])
GO
ALTER TABLE [dbo].[Cuenta]  WITH CHECK ADD FOREIGN KEY([ClienteId])
REFERENCES [dbo].[Cliente] ([Id])
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD FOREIGN KEY([CuentaId])
REFERENCES [dbo].[Cuenta] ([Id])
GO


